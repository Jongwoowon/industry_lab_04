package ictgradschool.industry.lab04.ex04;

import ictgradschool.Keyboard;

/**
 A game of Rock, Paper Scissors
 */
public class RockPaperScissors {

    public static final int ROCK = 1;
    // TODO Make similar constants for PAPER and SCISSORS, to improve readability of your code.
    final int PAPER = 2;
    final int SCISSORS = 3;

    public void start() {

        // TODO Write your code here which calls your other methods in order to play the game. Implement this
        // as detailed in the exercise sheet.


        System.out.print("Hi! What is your name? ");// •	Printing the prompt and reading the input from the user for the name
        String name = Keyboard.readInput();


        int playerChoice = 1;

        while (playerChoice != 0) {

        System.out.print("1. Rock\n2. Paper\n3. Scissors \n4. Quit\n      Enter choice: "); // •	Printing the prompt and reading the input from the user for the choice of rock, scissors, paper, or quit
         playerChoice = Integer.parseInt(Keyboard.readInput());



            System.out.println();

            if (playerChoice == 4) {
                System.out.println("Goodbye " + name + ". Thank you for playing :)");
                break;
            } else {

                displayPlayerChoice(name, playerChoice); //display the player choice
                String computerName = "The computer";
                int computerChoice = (int) ((Math.random() * 3) + 1);

                displayPlayerChoice(computerName, computerChoice); //display the player choice

                if (userWins(playerChoice, computerChoice)) {
                    System.out.println(name + " wins because " + getResultString(playerChoice, computerChoice));
                } else if (playerChoice == computerChoice) {
                    System.out.println(getResultString(playerChoice, computerChoice));
                } else {
                    System.out.println("The computer wins because " + getResultString(playerChoice, computerChoice));
                }

                // determine who wins, and display the results
                // •	Printing a message to display the goodbye message when the user chooses to quit the application

            }

        }
    }

    public void displayPlayerChoice(String name, int choice) {
        // TODO This method should print out a message stating that someone chose a particular thing (rock, paper or scissors)
        // System.out.print("Please choose between\n ROCK - 1\n PAPER - 2\n ROCK - 3");

        //The displayPlayerChoice() method has one String parameter and one int parameter.
        // Complete the method so that it prints one line of output.
        // The first part of the output is obtained from the String parameter.
        // The second part depends on the second int parameter (which you should compare with the global constants).
        // Depending on what value the user has entered, the second part of the output will be either: “ chose scissors”, “ chose rock”, or “ chose paper”.

        String printChoice = "";

        if (choice == 1) {
            printChoice = "Rock";
        } else if (choice == 2) {
            printChoice = "Paper";
        } else if (choice == 3) {
            printChoice = "Scissors";
        }
        System.out.println(name + " chose " + printChoice);
    }

    public boolean userWins(int playerChoice, int computerChoice) {
        // TODO Determine who wins and return true if the player won, false otherwise.

        //The userWins() method has two int parameters and returns a boolean.
        // The first int parameter is the user’s choice and the second int parameter is the computer’s choice.
        // The method returns true if the user choice beats the computer choice, otherwise the method returns false.

        if (playerChoice == PAPER) {
            if (computerChoice == ROCK) {
                return true;
            }
        }
        if (playerChoice == ROCK){
            if (computerChoice == SCISSORS){
                return true;
            }
        }
        if (playerChoice == SCISSORS){
            if (computerChoice == PAPER){
                return true;
            }
        }
        return false;
    }

    public String getResultString(int playerChoice, int computerChoice) {

        final String PAPER_WINS = "paper covers rock";
        final String ROCK_WINS = "rock smashes scissors";
        final String SCISSORS_WINS = "scissors cut paper";
        final String TIE = " you chose the same as the computer";

        // use true false statement

        // TODO Return one of the above messages depending on what playerChoice and computerChoice are.

        if (playerChoice == 1 && computerChoice == 3){ //    final String ROCK_WINS = "rock smashes scissors";
            return ROCK_WINS;
        }
        else if (playerChoice == 2 && computerChoice == 1){//    final String SCISSORS_WINS = "scissors cut paper";
            return PAPER_WINS;
        }
        else if (playerChoice == 3 && computerChoice ==2) {
            return SCISSORS_WINS;
        }
        else if (computerChoice == 1 && playerChoice == 3){ //    final String ROCK_WINS = "rock smashes scissors";
            return ROCK_WINS;
        }
        else if (computerChoice == 2 && playerChoice == 1){//    final String SCISSORS_WINS = "scissors cut paper";
            return PAPER_WINS;
        }
        else if (computerChoice == 3 && playerChoice ==2) {
            return SCISSORS_WINS;
        }
        else {//    final String TIE = " you chose the same as the computer";
                return TIE;
            }

        }



    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        RockPaperScissors ex = new RockPaperScissors();
        ex.start();

    }
}
