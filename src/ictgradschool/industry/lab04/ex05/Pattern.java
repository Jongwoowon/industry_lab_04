package ictgradschool.industry.lab04.ex05;

/**
 * Created by jwon117 on 14/03/2017.
 */
public class Pattern {

    private int repetitions;
    private char symbol;

    public Pattern(int repetitions, char symbol) {
        this.repetitions = repetitions;
        this.symbol = symbol;
    }



    public String toString(){
        String hashCounter = "";
        for (int i = 0; i < repetitions; i++){
            hashCounter = hashCounter + symbol;
        }
        return hashCounter;
    }


    public void setNumberOfCharacters(int repetitions) {
        this.repetitions = repetitions;
 //setting the class Pattern and the parameters to be used by the Pattern


        //Pattern sideOfFirstLine = new Pattern(7, '#');
        //Pattern sideOfLine = new Pattern(7, '~');
        //Pattern middle = new Pattern(1, '.');
    }

    public int getNumberOfCharacters() {
        return repetitions;
    }

}
