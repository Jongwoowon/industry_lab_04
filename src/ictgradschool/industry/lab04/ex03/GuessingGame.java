package ictgradschool.industry.lab04.ex03;

import ictgradschool.Keyboard;

/**
 * A guessing game!
 */
public class GuessingGame {

    public void start() {

        // TODO Write your code here.
        int goal = (int)(Math.random() * 100 + 1); //1. •	Generate a random number between 1 and 100 and store in a variable named goal (0.0 - 99)
        int guess; //2. •	Declare a variable named guess
        guess = 0; //3. •	Initialise guess to 0
        while (guess != goal) {//4. •	While the user’s guess is not correct (i.e. while guess != goal):
            System.out.print("Please enter your guess: ");//       o	Ask the user to enter their guess
            guess = Integer.parseInt(Keyboard.readInput());//       o	Store the guess in the guess variable
            if (guess > goal) {//       o	If the guess is greater than the goal, print “Too high, try again”
                System.out.println("Too high, try again");
            }
            else if (guess < goal){
                System.out.println("Too low, try again");//       o	Else if the guess is less than the goal, print “Too low, try again”
            }
            else {
                System.out.println("Perfect! ");//       o	Else print the message “Perfect!!”
            }

        }
        System.out.print("Goodbye");//5. •	Print “Goodbye”
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
